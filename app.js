let min = 0,
	max = 3,
	winningScoreHome = getRandomNum(min, max),
	winningScoreAway = getRandomNum(min, max),
	typesLeft = 3;

const gameWrapper = document.querySelector('#game'),
	  minNum = document.querySelector('.min-num'),
	  maxNum = document.querySelector('.max-num'),
	  guessBtn = document.querySelector('#guess-btn'),
	  typeInputHome = document.querySelector('#guess-input-home'),
	  typeInputAway = document.querySelector('#guess-input-away'),
	  message = document.querySelector('.message');

minNum.textContent = min;
maxNum.textContent = max;

typeInputHome.focus();

function submitOnEnter(e) {
	e.preventDefault;
	if(e.keyCode === 13) {
		guessBtn.click();
	}
}

typeInputAway.addEventListener('keyup', submitOnEnter);

typeInputHome.addEventListener('keyup', function(e) {
	if(typeInputHome.value !== '' && typeInputHome.value < max) { 
		typeInputAway.focus();
	}
});

gameWrapper.addEventListener('mousedown', function(e) {
	if(e.target.classList.contains('play-again')) {
		guessBtn.className -= ' play-again';
		window.location.reload();
	}
});

guessBtn.addEventListener('click', function(e) {
	let guessHome = parseInt(typeInputHome.value);
	let guessAway = parseInt(typeInputAway.value);

	if(isNaN(guessHome) || guessHome < min || guessHome > max) {
		setMessage(`Please enter a number between ${min} and ${max}`, '#B32121');
		return false;
	} 
	if(isNaN(guessAway) || guessAway < min || guessAway > max) {
		setMessage(`Please enter a number between ${min} and ${max}`, '#B32121');
		return false;
	}

	if(guessHome === winningScoreHome && guessAway === winningScoreAway) {
		gameOver(true, `You won, the score is ${winningScoreHome} : ${winningScoreAway}`);
	} else {
		typesLeft -= 1;

		if(typesLeft === 0) {
			gameOver(false, `You lost,  the score is ${winningScoreHome} : ${winningScoreAway}`);

		} else {
			setMessage(`Wrong guess. You have still ${typesLeft} chances`, '#B32121');
			typeInputHome.style.borderColor = '#B32121';
			typeInputHome.value = '';
			typeInputAway.style.borderColor = '#B32121';
			typeInputAway.value = '';
			typeInputHome.focus();
		}
	}
});

function gameOver(won, msg) {
	let color;
	won === true ? color = 'green' : color = '#B32121';
	setMessage(msg);
	
	typeInputHome.disabled = true; 
	typeInputHome.style.borderColor = color;
	typeInputAway.disabled = true; 
	typeInputAway.style.borderColor = color;
	message.style.color = color;
	guessBtn.value = 'Play Again';
	guessBtn.className += ' play-again';
}

function getRandomNum() {
	return Math.floor(Math.random()*(max-min+1)+min); 
}

function setMessage(msg, color) {
	message.style.color = color;
	message.textContent = msg;
}